﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cineEstrella
{
    public partial class ViewReport : Form
    {
        AccesoDatos dato = new AccesoDatos(@"Provider=SQLOLEDB;Data Source=MAKA-PC;Integrated Security=SSPI;Initial Catalog=CINE_ESTRELLAS");
        public ViewReport()
        {
            InitializeComponent();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            ReportePeliculas rp = new ReportePeliculas();
            crystalReportViewer.ReportSource = rp;
            crystalReportViewer.Refresh();
            crystalReportViewer.Show();
        }

        private void btnImpriFiltro_Click(object sender, EventArgs e)
        {
            ReportePeliculas rpo = new ReportePeliculas();

            string query = Consulta(cboClasi.SelectedIndex, cboVersion.SelectedIndex, cboGenero.SelectedIndex);


            rpo.SetDataSource(dato.consultar(query));
            crystalReportViewer.ReportSource = rpo;
            crystalReportViewer.Refresh();
            crystalReportViewer.Show();
            cboClasi.SelectedIndex = -1;
            cboGenero.SelectedIndex = -1;
            cboVersion.SelectedIndex = -1;

        }

        public void cargarCombos(ComboBox combo, string nombreTabla)
        {
            DataTable tabla = new DataTable();
            tabla = dato.consultarTabla(nombreTabla);
            combo.DataSource = tabla;
            combo.ValueMember = tabla.Columns[0].ColumnName;
            combo.DisplayMember = tabla.Columns[1].ColumnName;
            combo.DropDownStyle = ComboBoxStyle.DropDownList;
            combo.SelectedIndex = -1;
        }

        private void ViewReport_Load(object sender, EventArgs e)
        {
            this.cargarCombos(cboClasi, "clasificaciones");
            this.cargarCombos(cboGenero, "generos_peliculas");
            this.cargarCombos(cboVersion, "versiones");
        }

        public string Consulta(int x, int y, int z)
        {
            string qfinal = "";
            int a = x;
            int b = y;
            int c = z;
            
            if (a != -1)
            {
                if (b == -1 && c == -1)
                {
                    qfinal = "Select * from peliculas where id_clasificacion =" + (a + 1);
                }
                else if (b != -1 && c == -1)
                {
                    qfinal = "Select* from peliculas where id_clasificacion = " + (a + 1) + "and id_version=" + (b + 1);
                }
                else if (b == -1 && c != -1)
                {
                    qfinal = "Select* from peliculas where id_clasificacion = " + (a + 1) + "and idgenero_pelicula=" + (c + 1);
                }
                else if ( b != -1 && c != -1)
                {
                    qfinal = "Select * from peliculas where id_clasificacion = " + (a + 1) + "and id_version = "+ (b+1) +
                        "and idgenero_pelicula = " + (c + 1);
                        //"exec ListaPeliculas @Clafi= " + (a + 1) + ", @Verci= " + (b + 1) + ",@Genero= " + (c + 1);
                }
            }
            if (b != -1)
            {
                if (a == -1 && c == -1)
                {
                    qfinal = "Select * from PELICULAS where id_version =" + (b + 1);
                }
                else if (a == -1 && c != -1)
                {
                    qfinal = "Select* from peliculas where id_version = " + (b + 1) + "and idgenero_pelicula=" + (c + 1);
                }
            }
            if (c != -1)
            {
                if (a == -1 && b == -1)
                {
                    qfinal = "Select * from peliculas where idgenero_pelicula =" + (c + 1);
                }
            }
            if (a == -1 && b == -1 && c == -1)
            {
                qfinal = "Select * from Peliculas ";

            }
            return qfinal;
        }

    }  
}   

